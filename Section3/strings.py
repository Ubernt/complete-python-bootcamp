a = "Hey {}".format("Jon")
print(a)

b = "Hello world"
print(b[::])
print("Otherway around: " + b[::-1])

c = "abcdefghijk"
print("Take every two character: " + c[::2])

c = "abcdefghijk"
print("Take only cd from string: " + c[2:4])

d = "abcdefghijk"
print("Take every 2 characters from pos 1 to 8: " + d[1:8:2])

name = "Jon"
f = f"String inperpolation {name}"
print(f)
