my_list = [1,2,3]
my_list_mix = ['STRING', 100, 23.2]
print(my_list_mix[0])
print(my_list_mix[1])
print(my_list_mix[2])
my_list_mix[2] = "asdassadadssaddsa"
print(my_list_mix[2])
new_list = my_list + my_list_mix
print(new_list[4])
my_list.append(4) # lisää uusi numero listan loppuun
print(my_list[3])
jon_list = [5,6]
my_list.extend(jon_list)
print(my_list[5])
print("pop: " + str(my_list.pop())) # otetaan listan lopusta pois numero 6
print(my_list[4]) # listan pohjalla on 5
#otetaan eka pois
print("pop: " + str(my_list.pop(0))) # otetaan listan lopusta pois numero 6
print("my_list[0]: " + str(my_list[0])) #

alpha_list = ["a", "g", "u", "x", "b", "k"]
alpha_list.sort() # tämä ei palauta mitään vaan sekoittaa sisäisesti
print(alpha_list[0])
print(alpha_list[1])
print(alpha_list[2])
print(alpha_list[3])
alpha_copy_list = alpha_list
alpha_list[0] = "s"
print(alpha_list[0])
print(alpha_copy_list[0])